;; Copyright (c) 2020 Akshay Srinivasan <akssri@vakra.xyz>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package #:upakarana-tests)

(5am:def-suite :fibonacci-heap :in test-suite)
(5am:in-suite :fibonacci-heap)

(5am:test fibonacci-heap-test
  (let* ((N 1000)
	 (heap (make-instance 'u.fheap:fibonacci-heap))
	 (order (u.fheap:order heap))
	 (table (make-hash-table)))
    ;; insert
    (iter (for ii below N)
      (letv* ((_ node (u.fheap:push (random 1d0) heap)))
	(setf (gethash ii table) node)))
    ;; random update
    (iter (for ii below N)
      (u.fheap:update-node (random 1d0) (gethash ii table) heap))
    ;; test
    (is
     (iter (repeat N)
       (with prev-min = nil)
       (let ((cur-min (u.fheap:pop heap)))
	 (never (and prev-min (not (funcall order prev-min cur-min))))
	 (setf prev-min cur-min))))))

#+nil
(time ;; 4.403s
 (letv* ((n 1000000)
	 (heap (make-instance 'u.fheap:fibonacci-heap)))
   (iter (for ii below n)
     (u.fheap:push (random 1d0) heap))
   (iter (for ii below n)
     (u.fheap::pop heap))))
