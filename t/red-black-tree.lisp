;; Copyright (c) 2020 Akshay Srinivasan <akssri@vakra.xyz>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package #:upakarana-tests)

(5am:def-suite :red-black-tree :in test-suite)
(5am:in-suite :red-black-tree)

(defun tree-pre-order (z)
  (when z
    (let ((stack (list z)))
      (iter (while stack)
	(letv* ((x (cl:pop stack)))
	  (collect x)
	  (if-let (rx (u.rbt::node-right x)) (cl:push rx stack))
	  (if-let (lx (u.rbt::node-left x)) (cl:push lx stack)))))))

(5am:test insert-fixup-case-2-3
  (letv* (((a b c d x y z) (mapcar #'(lambda (x) (u.rbt::make-node :car x))
				   '(a b c d x y z))))
    (setf (u.rbt::node-color z) nil
	  (u.rbt::node-left-s z) x
	  (u.rbt::node-right-s z) d
	  ;;
	  (u.rbt::node-color x) t
	  (u.rbt::node-left-s x) a
	  (u.rbt::node-right-s x) y
	  ;;
	  (u.rbt::node-color y) t
	  (u.rbt::node-left-s y) b
	  (u.rbt::node-right-s y) c)
    (u.rbt::insert-fixup y)
    (is (equal (mapcar #'u.rbt::node-car (tree-pre-order (u.rbt::tree-root a)))
	       '(y x a b z c d)))))
;;
(defun rb-prop-2-4p (rb)
  (if (not (u.rbt::root rb)) t
      (let ((stack (list (u.rbt::root rb))))
	(and (not (u.rbt::node-parent (u.rbt::root rb))) (not (u.rbt::node-color (u.rbt::root rb)))
	     (iter (while stack)
	       (letv* ((x (cl:pop stack)))
		 (if-let (lx (u.rbt::node-left x)) (cl:push lx stack))
		 (if-let (rx (u.rbt::node-right x)) (cl:push rx stack))
		 (when (u.rbt::node-color x)
		   (always (and (not (if-let (lx (u.rbt::node-left x)) (u.rbt::node-color lx)))
				(not (if-let (rx (u.rbt::node-right x)) (u.rbt::node-color rx))))))))))))

(defun rb-prop-5p (rb)
  (if (not (u.rbt::root rb)) t
      (let ((stack (list (list (u.rbt::root rb) 0)))
	    (path-black-count nil))
	(iter (while stack)
	  (letv* (((nd n-black) (cl:pop stack)))
	    (if (not (u.rbt::node-color nd)) (incf n-black))
	    (if (u.rbt::node-left nd)
		(cl:push (list (u.rbt::node-left nd) n-black) stack)
		(progn
		  (if (not path-black-count)
		      (setf path-black-count n-black)
		      (always (= n-black path-black-count)))))
	    (if (u.rbt::node-right nd)
		(cl:push (list (u.rbt::node-right nd) n-black) stack)
		(progn
		  (if (not path-black-count)
		      (setf path-black-count n-black)
		      (always (= n-black path-black-count))))))))))

(5am:test red-black-tree-test
  (letv* ((n 1000)
	  (rb (make-instance 'u.rbt::red-black-tree))
	  (tbl (make-hash-table)))
    ;; test invariance of red-black tree properties
    (let ((prop-2-4p t) (prop-5p t))
      (iter (for ii below (* 2 n))
	 (let ((x (random 1d0)))
	   (letv* ((_ nd (u.rbt::push x rb)))
	     (setf (gethash ii tbl) nd))
	   (setf prop-5p (and prop-5p (rb-prop-5p rb))
		 prop-2-4p (and prop-2-4p (rb-prop-2-4p rb)))))
      (is (progn prop-2-4p))
      (is (progn prop-5p)))
    ;; delete
    (let ((prop-2-4p t) (prop-5p t))
      (iter (for ii below n)
	(u.rbt::delete-node (gethash ii tbl) rb)
	(setf prop-5p (and prop-5p (rb-prop-5p rb))
	      prop-2-4p (and prop-2-4p (rb-prop-2-4p rb))))
      ;; test invariance of red-black tree properties
      (is (progn prop-2-4p))
      (is (progn prop-5p)))
    ;; pop
    (let ((prop-2-4p t) (prop-5p t)
	  (order (u.rbt::order rb)))
      ;; test ordering
      (is
       (iter (for ii below n)
	 (with prev-min = nil)
	 (let ((cur-min (u.rbt::pop rb)))
	   (never (and prev-min (not (funcall order prev-min cur-min))))
	   (setf prev-min cur-min))
	 (setf prop-5p (and prop-5p (rb-prop-5p rb))
	       prop-2-4p (and prop-2-4p (rb-prop-2-4p rb)))))
      ;; test invariance of red-black tree properties
      (is (progn prop-2-4p))
      (is (progn prop-5p)))))

#+nil
(time ;; 1.744s (C++: 2.20s)
 (letv* ((n 1000000)
	 (rb (make-instance 'u.rbt:red-black-tree)))
   (iter (for ii below n)
     (u.rbt::push (random 1d0) rb))
   (iter (for ii below n)
     (u.rbt::pop rb))))
