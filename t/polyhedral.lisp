;; Copyright (c) 2020 Akshay Srinivasan <akssri@vakra.xyz>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package #:upakarana-tests)

(5am:def-suite :polyhedral :in test-suite)
(5am:in-suite :polyhedral)

(5am:test fm-test
  (letv* ((A tbl (u.polyhedral:read-fm '((<= 0 i 10)
					 (<= 0 j 5)
					 (<= (- i j) 0))))
	(A-nxt sol (u.fm:fourier-motzkin 1 A)))
  (flet ((inequation (x) (list (coerce (u.fm:inequation-row x) 'list) (u.fm:inequation-op x))))
    ;; eliminated variable solution
    (is
     (equal
      (map 'list #'inequation sol)
      '(((0 1 5) :<=)  ;; j <= 5
	((0 -1 0) :<=) ;; j >= 0
	((1 -1 0) :<=))))
    ;; sub problem
    (is
     (equal
      (map 'list #'inequation A-nxt)
      '(((-1 0 0) :<=) ;; i >= 0
	((1 0 5) :<=)))))))

(5am:test ilp-test
  (letv* ((eps (scale-float 1d0 -46))
	  ((c a op b) (u.polyhedral:read-lp
		       '(+ 0 x-1 (* 2 x-2) (* 5/10 x-3) (* 2/10 x-4) x-5 (* -6/10 x-6))
		       ;; constraints
		       '((>= (+ x-1 (* 2 x-2)) 1)
			 (>= (+ x-1 x-2 (* 3 x-6)) 1)
			 (>= (+ x-1 x-2 x-6) 1)
			 (>= (+ x-3 (* -3 x-4)) 1)
			 (>= (+ x-3 (* -2 x-4) (* -5 x-5)) 1)
			 (>= (+ x-4 (* 3 x-5) (* -4 x-6)) 1)
			 (>= (+ x-2 x-5 x-6) 1)
			 (<= 0 x-1 10)
			 (<= 0 x-2 10)
			 (<= 0 x-3 10)
			 (<= 0 x-4 10)
			 (<= 0 x-5 10)
			 (<= 0 x-6 10))))
	  (integer-variables (coerce (vector 0 1 2 3 4 5) '(simple-array u.splx:simplex-itype (*)))))
    (is (< (abs (- (u.splx:intlinprog c a op b :integer-constraint integer-variables) 42/10)) eps))))

(5am:test do-polyhedron-test
  (let ((ret nil))
    (u.polyhedral:do-polyhedron (ii (<= 1 ii 4)
		       (<= 1 jj 2)
		       (<= ii jj)
		       (= of (+ 2 (* jj 3) (* ii 2))))
       (u.polyhedral:do-polyhedron (jj)
	 (u.polyhedral:do-polyhedron (of)
	   (push (list ii jj of) ret))))
    (is (equal (reverse ret)
	       '((1 1 7)
		 (1 2 10)
		 (2 2 12))))))
