;; Copyright (c) 2020 Akshay Srinivasan <akssri@vakra.xyz>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package #:upakarana-tests)

(5am:def-suite :random :in test-suite)
(5am:in-suite :random)

(5am:test jb-normal-test
  (letv* ((n 100000)
	  (samples (make-array n :element-type 'double-float)))
    (iter (for ii below n) (setf (aref samples ii) (u.random:normal)))
    (let* ((m1 (* (/ n) (iter (for x in-vector samples) (summing x))))
	   (m2 (* (/ n) (iter (for x in-vector samples) (summing (expt (- x m1) 2)))))
	   (m3 (* (/ n) (iter (for x in-vector samples) (summing (expt (- x m1) 3)))))
	   (m4 (* (/ n) (iter (for x in-vector samples) (summing (expt (- x m1) 4)))))
	   (g1 (/ m3 (expt m2 3/2)))
	   (g2 (- (/ m4 m2 m2) 3))
	   (jb (* (/ n 6) (+ (* g1 g1) (* 1/4 g2 g2))))
	   (p (- 1 (- 1 (exp (/ (- jb) 2))))))
      (is (< 0.05d0 p)))))
