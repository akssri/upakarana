;; Copyright (c) 2020 Akshay Srinivasan <akssri@vakra.xyz>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package #:upakarana-red-black-tree)
;; Based on CLR, Chapter 13.

(defstruct node
  car
  (left nil :type (or null node))
  (right nil :type (or null node))
  (parent nil :type (or null node))
  (color nil :type boolean))

(defmethod print-object ((x node) stream)
  (print-unreadable-object (x stream :type t)
    (format stream "car: ~A" (node-car x))))

(declaim (inline (setf node-left-s)))
(defun (setf node-left-s) (value node)
  (when value
    (setf (node-parent value) node))
  (setf (node-left node) value))

(declaim (inline (setf node-right-s)))
(defun (setf node-right-s) (value node)
  (when value
    (setf (node-parent value) node))
  (setf (node-right node) value))

(declaim (inline set-node-parent-s))
(defun set-node-parent-s (y px x)
  (if (eq x (node-left px))
      (setf (node-left-s px) y)
      (setf (node-right-s px) y)))

;;    |                        |
;;    x         L(x) ->        y
;;   / \        <- R(y)       / \
;;  a   y                    x   c
;;     / \                  / \
;;    b   c                a   b

(declaim (inline left-rotate))
(defun left-rotate (x)
  (declare (type node x))
  (let* ((px (node-parent x)) (y (node-right x)) (b (node-left y)))
    (setf (node-right-s x) b
	  (node-left-s y) x)
    (if px
	(set-node-parent-s y px x)
	(setf (node-parent y) px))
    y))

(declaim (inline right-rotate))
(defun right-rotate (y)
  (declare (type node y))
  (let* ((py (node-parent y)) (x (node-left y)) (b (node-right x)))
    (setf (node-left-s y) b
	  (node-right-s x) y)
    (if py
	(set-node-parent-s x py y)
	(setf (node-parent x) py))
    x))
;;
(defmacro with-lr-symmetry (left-pred &body left-body
			    &aux (lr-pairs '((node-left node-right)
					     (node-left-s node-right-s)
					     (left-rotate right-rotate))))
  (let ((right-body (maptree #'(lambda (tree)
				 (if-let (pair (and (atom tree) (find tree lr-pairs :test #'member)))
				   (values-list (remove tree pair))
				   (values tree t)))
			     left-body)))
    `(if ,left-pred
	 (progn ,@left-body)
	 (progn ,@right-body))))

;;
(defun tree-left* (z)
  (if-let (lz (and z (node-left z)))
    (tree-left* lz)
    z))

(defun tree-right* (z)
  (if-let (rz (and z (node-right z)))
    (tree-right* rz)
    z))

(defun tree-prev (z)
  (if-let (lz (node-left z))
    (tree-right* lz)
    (labels ((prev (x)
	       (when-let (px (node-parent x))
		 (if (eq x (node-right px)) px
		     (prev px)))))
      (prev z))))

(defun tree-next (z)
  (if-let (rz (node-right z))
    (tree-left* rz)
    (labels ((succ (x)
	       (when-let (px (node-parent x))
		 (if (eq x (node-left px)) px
		     (succ px)))))
      (succ z))))

(defun tree-root (x)
  (if-let (px (node-parent x)) (tree-root px) x))

(declaim (inline bisect-node))
(defun bisect-node (x tree &key (order #'<))
 "(BISECT-NODE x root &key [order #'<]) => z, insert-node, insert-place
  finds the left-most node @arg{z} in the descendents of z st., (not (order (node-car z) x)) is satisfied; else returns NIL
  assumptions: z is a BST under @arg{order}.

  the auxillary outputs @arg{insert-node}, @arg{insert-place}, indicate a potential insertion place."
  (declare (type node tree))
  (iter (with min-node = nil)
    (for root initially tree then
	 (if (not (funcall order (node-car root) x))
	     (if-let (lx (node-left root))
	       (prog1 lx (setf min-node root))
	       (return (values root root nil)))
	     (if-let (rx (node-right root))
	       rx
	       (return (values min-node root t)))))))
;;
(defclass red-black-tree ()
  ((root :initform nil :reader root)
   (size :initform 0 :reader size)
   (order :initarg :order :reader order :initform #'<)))

;;INSERT
(defun push (x tree &key test)
  "(PUSH obj rbt) => obj, node
  INSERT. Insert object into tree; O(log n)."
  (declare (type red-black-tree tree))
  (if (not (root tree))
      (let ((x-node (make-node :car x :color nil)))
	(setf (slot-value tree 'root) x-node)
	(incf (slot-value tree 'size))
	(values x x-node))
      (letv* ((min-node px-node rightp (bisect-node x (root tree) :order (order tree))))
	(if (and test min-node (funcall test (node-car min-node) x))
	    (values (node-car min-node) nil)
	    (let ((x-node (make-node :car x :color t)))
	      (with-lr-symmetry (not rightp)
		(setf (node-left-s px-node) x-node))
	      (when-let (new-root (insert-fixup x-node))
		(setf (slot-value tree 'root) new-root))
	      (incf (slot-value tree 'size))
	      (values x x-node))))))

(defun insert-fixup (z)
  (declare (type node z))
  (if-let (pz (node-parent z))
    (when (node-color pz)
      (let ((ppz (node-parent pz)))
	(with-lr-symmetry (eq pz (node-left ppz))
	  (cond
	    ;; case 1
	    ((and (node-right ppz) (node-color (node-right ppz)))
	     (setf (node-color ppz) t
		   (node-color pz) nil
		   (node-color (node-right ppz)) nil)
	     (insert-fixup ppz))
	    ;; case 2
	    ((eq z (node-right pz))
	     (insert-fixup (node-left (left-rotate pz))))
	    ;; case 3
	    (t (let ((y (right-rotate ppz)))
		 (setf (node-color (node-right y)) t
		       (node-color y) nil)
		 ;; new root
		 (if (not (node-parent y)) y)))))))
    (prog1 z ;; new root
      (setf (node-color z) nil))))

;;DELETE
(defun delete-node (z tree)
  "(DELETE-NODE x rbt) => x
  delete the node @arg{x} from @arg{rbt}; O(log n)."
  (declare (type red-black-tree tree)
	   (type node z))
  (assert (eq (tree-root z) (root tree)) nil "node not in tree")
  (letv* ((x need-fixp x-sentinelp (splice-out-node z tree)))
    (when need-fixp
      (if-let (new-root (delete-fixup x nil))
	(setf (slot-value tree 'root) new-root)))
    (when x-sentinelp
      (set-node-parent-s nil (node-parent x) x)))
  (decf (slot-value tree 'size))
  ;; clear z
  (setf (node-left z) nil (node-right z) nil (node-parent z) nil (node-color z) nil)
  z)

(defun splice-out-node (z tree)
  (cond
    ((and (node-left z) (node-right z))
     (letv* ((y (tree-left* (node-right z))) (y-redp (node-color y))
	     (cy sentinelp (or (node-right y)
			       (values (setf (node-right-s y) (make-node :car nil :color nil)) t))))
       ;; splice child of y to py
       (set-node-parent-s cy (node-parent y) y)
       ;; splice y in place of z (CLR flips keys inplace)
       (setf (node-color y) (node-color z) (node-left-s y) (node-left z) (node-right-s y) (node-right z))
       (if-let (pz (node-parent z))
	 (set-node-parent-s y pz z)
	 (setf (node-parent y) nil
	       (slot-value tree 'root) y))
       (values cy (not y-redp) sentinelp)))
    ((node-parent z)
     (letv* ((cz sentinelp (or (node-left z) (node-right z)
			       (values (setf (node-right-s z) (make-node :car nil :color nil)) t))))
       ;; splice child of z to pz
       (set-node-parent-s cz (node-parent z) z)
       (values cz (not (node-color z)) sentinelp)))
    (t
     (letv* ((cz (or (node-left z) (node-right z))))
       (setf (slot-value tree 'root) cz)
       (if cz
	   (setf (node-parent cz) nil
		 (node-color cz) nil))
       (values cz)))))

(defun delete-fixup (x new-root)
  (declare (type node x))
  (if-let (px (and (not (node-color x)) (node-parent x)))
    (with-lr-symmetry (eq x (node-left px))
      (let ((w (node-right px)))
	(declare (type node w))
	;; case 1
	(when (node-color w)
	  (setf (node-color w) nil
		(node-color px) t)
	  (left-rotate px)
	  ;; !!short-cut for (delete-fixup x ...)
	  (if (not (node-parent w)) (setf new-root w))
	  (setf w (node-right px)))
	;; case 2
	(if (and (not (if-let (lw (node-left w)) (node-color lw)))
		 (not (if-let (rw (node-right w)) (node-color rw))))
	    (progn
	      (setf (node-color w) t)
	      (delete-fixup px new-root))
	    (progn
	      ;; case 3
	      (when (not (if-let (rw (node-right w)) (node-color rw)))
		(setf (node-color (node-left w)) nil
		      (node-color w) t)
		(right-rotate w)
		;; !!short-cut for (delete-fixup x ...)
		(setf w (node-right px)))
	      ;; case 4
	      (setf (node-color w) (node-color px)
		    (node-color px) nil
		    (node-color (node-right w)) nil)
	      (left-rotate px)
	      ;; !!return
	      (if (not (node-parent w)) (setf new-root w))
	      new-root))))
    (progn
      (setf (node-color x) nil)
      new-root)))

;;POP
(defun pop (tree)
  "(pop rbt) => extreme-value, extreme-node
  This operation extricates the extremum node from @arg{rbt}; O(log n)."
  (declare (type red-black-tree tree))
  (let* ((xnode (tree-left* (root tree))))
    (delete-node xnode tree)
    (values (node-car xnode) xnode)))

;;PRINT
(defun print-tree (str z)
  (if z
      (letv* ((z-str (splitseq #'(lambda (s) (char= s #\Newline))
			       (with-output-to-string (str)
				 (if (node-color z)
				     (format str (format nil " ____~%|~4F|~%/¯¯¯¯\\" (node-car z)))
				     (format str "~% ~4F~%/    \\" (node-car z))))))
	      (z-w (reduce #'max z-str :key #'length :initial-value 0))
	      ;;
	      (l-str (splitseq #'(lambda (s) (char= s #\Newline))
			       (with-output-to-string (str)
				 (print-tree str (node-left z)))))
	      (l-w (reduce #'max l-str :key #'length :initial-value 0))
	      ;;
	      (r-str (splitseq #'(lambda (s) (char= s #\Newline))
			       (with-output-to-string (str)
				 (print-tree str (node-right z)))))
	      (r-w (reduce #'max r-str :key #'length :initial-value 0)))
	(iter (for zsi in z-str)
	  (format str "~/u.rbt::<sp>/~a~/u.rbt::<sp>/~%"  l-w zsi (+ (- z-w (length zsi)) r-w)))
	(iter (while (or l-str r-str))
	  (let ((li (or (cl:pop l-str) "")) (ri (or (cl:pop r-str) "")))
	    (format str "~/u.rbt::<sp>/~a~/u.rbt::<sp>/~a~/u.rbt::<sp>/~%" (- l-w (length li)) li z-w ri(- r-w (length ri))))))
      (format str "NIL")))

(defun splitseq (pred seq)
  (iter (with end = -1)
    (for ii below (1+ (length seq)))
    (when (or (= ii (length seq)) (funcall pred (aref seq ii)))
      (when (/= (1+ end) ii)
	(collect (subseq seq (1+ end) ii)))
      (setf end ii))))

(defun <sp> (str arg colonp at-signp)
  (iter (repeat arg) (princ #\Space str)))
