;; Copyright (c) 2020 Akshay Srinivasan <akssri@vakra.xyz>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package #:upakarana-fibonacci-heap)

;; Based on CLR, Chapter 19. CLR refers to 'key' as the values of the nodes (node.car) that are
;; to be compared. The term key (node.key) is used here in the context of key-value store, and
;; is meant for keeping track of vertices etc. in graph-algorithms.

(defstruct (node
	    (:include dcons
	     (rdc nil :type (or null node))
	     (cdr nil :type (or null node))))
  ;;        ____________         _______
  ;;       /            \       /       \
  ;; [.|.| . ] [dcar | drdc | dcdr] [.| . |.]
  (parent nil :type (or null node))
  (children nil :type (or null node))
  (degree 0 :type fixnum)
  (markp nil :type boolean))

(defmethod print-object ((nd node) stream)
  (print-unreadable-object (nd stream :type t)
    (format stream "car: ~A, |children|: ~A" (dcar nd) (node-degree nd))))

(declaim (inline node))
(defun node (se1)
  "(NODE se1) => node
   se1: object held in the node
   index: auxillary pointer"
  (let ((nd (make-node :car se1)))
    (setf (drdc nd) nd (dcdr nd) nd)
    nd))

(declaim (inline dsplice!))
(defun dsplice! (rlst1 rlst2)
  ";; rlst1, rlst2 assumed to be d.l.l rings
   ;; (DSPLICE! |a - b - c - a| |x - y - z - x|)
   ;; => |a - b - c - x - y - z - a|"
  (if rlst2
      (let ((rend.1 (drdc rlst1))
	    (rend.2 (drdc rlst2)))
	(setf (dcdr rend.1) rlst2
	      (drdc rlst2) rend.1
	      (dcdr rend.2) rlst1
	      (drdc rlst1) rend.2)))
  rlst1)
;;
(defclass fibonacci-heap ()
  ((xnode :initform nil :reader xnode :documentation "extremum node")
   (size :initform 0 :reader size :documentation "number of elements in the heap")
   (order :initarg :order :reader order :initform #'< :documentation "ordering of the elements")))

(defmethod print-object ((fib fibonacci-heap) stream)
  (print-unreadable-object (fib stream :type t)
    (format stream "size: ~A" (size fib))))

(defun make-heap (seq &optional (order #'<))
  (let ((heap (make-instance 'fibonacci-heap :order order)))
    (map nil #'(lambda (x) (u.fheap:push x heap)) seq)
    heap))

;; MIN-KEY
(declaim (inline peek))
(defun peek (fib)
  "(PEEK fib) => NIL or (extreme-value, extremum-node-table-index)
  Return, if it exists, the extreme element and its corresponding node index in NODE-TABLE; O(1)."
  (when-let (min (xnode fib))
    (values (dcar min) min)))

;; INSERT-NODE
(defun push (obj fib &aux (node (node obj)))
  "(PUSH obj fib) => obj, node
  INSERT. Insert object into heap; O(1)."
  (declare (type fibonacci-heap fib))
  (insert-node node fib)
  (values obj node))

(defun insert-node (node fib) ;; O(1)
  (declare (type fibonacci-heap fib)
	   (type node node))
  (if-let ((root (xnode fib)))
    (progn
      (dsplice! node root)
      (if (funcall (order fib) (dcar node) (dcar root))
	  (setf (slot-value fib 'xnode) node)))
    (setf (slot-value fib 'xnode) node))
  (incf (slot-value fib 'size))
  node)

;; EXTRACT-MIN
(defun pop (fib)
  "(pop fib) => extreme-value
  EXTRACT-MIN. This operation extricates the extremum node from the Fibonacci heap @arg{fib}; O(log n).
  The delayed work of consolidating trees in the root list occurs within this function (CLR 3rd ed)."
  (declare (type fibonacci-heap fib))
  (when-let ((prev-xnode (xnode fib)))
    (when-let (xnode.children (node-children prev-xnode))
      (iter (for child on-dlist xnode.children until (drdc xnode.children)) (setf (node-parent child) nil))
      (dsplice! xnode.children prev-xnode))
    (dpop (slot-value fib 'xnode)) ;; remove prev-xnode from heap
    (decf (slot-value fib 'size))
    (cond
      ((= 0 (size fib)) (setf (slot-value fib 'xnode) nil))
      ((eql (xnode fib) (drdc (xnode fib))) nil) ;;single tree
      (t (consolidate fib)))
    ;; remove pointers
    (setf (dcdr prev-xnode) prev-xnode (drdc prev-xnode) prev-xnode (node-degree prev-xnode) 0
	  (node-children prev-xnode) nil (node-parent prev-xnode) nil (node-markp prev-xnode) nil)
    (values (dcar prev-xnode) prev-xnode)))

(defun consolidate (fib)
  (declare (type fibonacci-heap fib))
  (letv* ((root (xnode fib))
	  (order (slot-value fib 'order))
	  (degree-table (make-array (+ 2 (integer-length (size fib))) :initial-element nil)))
    (iter (for w on-dlist root until (drdc root))
      (repeat (size fib))
      (iter (for y in-vector degree-table with-index dd from (node-degree w)) (with x = w)
	(unless y
	  (setf (aref degree-table dd) x (node-degree x) dd)
	  (finish))
	;; exchange nodes if y < x
	(when (funcall order (dcar y) (dcar x)) (rotatef y x))
	(setf
	 ;; cut node y from root list
	 root (let ((root_y y)) (dpop root_y) root_y)
	 ;; make y a child of x
	 (node-parent y) x (node-markp y) nil (drdc y) y (dcdr y) y
	 (node-children x) (dsplice! y (node-children x))
	 (node-degree x) (1+ (node-degree x))
	 ;; update root-degree table
	 (aref degree-table dd) nil)))
    ;;update min
    (iter (for x on-dlist root until (drdc root))
      (with fmin = nil)
      (when (or (null fmin) (funcall order (dcar x) (dcar fmin))) (setf fmin x))
      (finally (setf (slot-value fib 'xnode) fmin))))
  (values))

;;
(defun %heap-memberp (x root)
  (when root
    (if-let (px (node-parent x)) (%heap-memberp px root)
	(iter (for ri on-dlist root until (drdc root))
	  (thereis (eq ri x))))))

;; DECREASE-KEY
(defun update-node (new x fib &aux (ord (order fib)))
 "(UPDATE-NODE new x fib) => new
  update the key of node @arg{x} to @arg{new}.
  if @arg{new} is of higher precedence, then performs O(1) update, else reinserts node in O(log n)."
  (declare (type fibonacci-heap fib)
	   (type node x))
  ;;(assert (%heap-memberp x (xnode fib)) nil "node not in the heap") ;; O(log n)
  (if (not (funcall ord (dcar x) new))
      (decrease-key (progn (setf (dcar x) new) x) fib) ;; O(1)     decrement
      (progn                                           ;; O(log n) reinsertion
	(delete-node x fib)
	(setf (dcar x) new)
	(insert-node x fib)))
  new)

(defun decrease-key (x fib &aux (ord (order fib)))
  (declare (type fibonacci-heap fib)
	   (type node x))
  (when (dcdr x)
    ;;cut node
    (when-let (y (node-parent x))
      (when (funcall ord (dcar x) (dcar y))
	(cut x y fib) (ccut y fib)))
    ;;update min
    (when (funcall ord (dcar x) (dcar (xnode fib)))
      (setf (slot-value fib 'xnode) x)))
  (values))

;; DELETE NODE
(defun delete-node (x fib)
  "(DELETE-NODE x fib) => x
  delete the node @arg{x} from @arg{fib}; O(log n)."
  (declare (type fibonacci-heap fib)
	   (type node x))
  (assert (%heap-memberp x (xnode fib)) nil "node not in the heap")
  (when (dcdr x)
    ;;cut node
    (when-let (y (node-parent x))
      (cut x y fib) (ccut y fib))
    ;;move to min & pop
    (setf (slot-value fib 'xnode) x)
    (upakarana-fibonacci-heap::pop fib))
  x)

;;
(defun cut (x xp fib)
  (declare (type fibonacci-heap fib)
	   (type node x xp))
  ;; remove x from the children of y
  (decf (node-degree xp))
  (setf (node-children xp) (let ((child_x x)) (dpop child_x) (if (= 0 (node-degree xp)) nil child_x)))
  ;; add x to the root list of fib
  (setf (node-parent x) nil (node-markp x) nil (dcdr x) x (drdc x) x)
  (dsplice! x (xnode fib))
  nil)

(defun ccut (y fib)
  (declare (type fibonacci-heap fib)
	   (type node y))
  (when-let ((z (node-parent y)))
    (if (node-markp y)
	(progn (cut y z fib) (ccut z fib))
	(setf (node-markp y) t)))
  nil)
