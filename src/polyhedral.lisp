(in-package :upakarana-polyhedral)

;; simplify
(defun simplify-1 (expr)
  (match expr
    ((list* (and op (or '+ '*)) (guard rest (some #'numberp (cdr rest))))
     (let ((coeff (reduce (symbol-function op) (remove-if-not #'numberp rest))))
       `(,op ,@(if (/= coeff (funcall (symbol-function op))) (list coeff))
	     ,@(remove-if #'numberp rest))))
    ((list '* (and a (symbol)) (and b (number))) (list '* b a))
    ((list '- a)  `(* -1 ,a))
    ((list* '- a rest) `(+ ,a ,@(mapcar #'(lambda (x) `(* -1 ,x)) rest)))
    ((list '1- a) `(+ -1 ,a))
    ((list '1+ a) `(+ 1 ,a))
    ((list (or '+ '*) a) a)
    ((list* '* 0 _) 0)
    ((list '*) 1) ((list '+) 0)
    ((list* '* 1 a) (list* '* a))
    ((list* '+ 0 a) (list* '+ a))
    (_ expr)))

(defun simplify-op (x)
  (ematch x
    ;; canonicalize
    ((list* (and op (or '+ '*)) (and a (not (number))) rest)
     (simplify-op (list* op (funcall (symbol-function op)) a rest)))
    ;; constant reduction
    ((list* (and op (or '+ '*)) (and a (number)) (and b (number)) rest)
     (simplify-op (list* op (funcall (symbol-function op) a b) rest)))
    ;; tree flatten
    ((list* (and op (or '+ '*)) (and a (number)) (list* (eql op) b-args) rest)
     (simplify-op (list* op a (append b-args rest))))
    ;; recurse
    ((list* (and op (or '+ '*)) (and a (number)) rest)
     (if (not rest) x
	 (let ((ret (simplify-op (list* op a (cdr rest)))))
	   (list* op (second ret) (simplify (car rest)) (cddr ret)))))))

(defun yc (f x)
  (let ((f.x (funcall f x)))
    (if (eql f.x x) x (yc f f.x))))

(defun simplify (expr)
  (let ((x (yc #'simplify-1 expr)))
    (match x
      ((list* (or '+ '*) _)
       (yc #'simplify-1 (simplify-op x)))
      (_ x))))

;; expand
(defun expand (expr)
  (match (simplify expr)
    ((list* '+ rest) (simplify `(+ ,@(mapcar #'expand rest))))
    ((list* '* rest) ;; expand
     (iter (for ei in rest)
       (match ei
	 ((list* '+ rest-i) ;; distribute
	  (return (simplify `(+ ,@(mapcar #'(lambda (x) (expand `(* ,x ,@(subseq rest 0 ii) ,@(subseq rest (1+ ii))))) rest-i))))))
       (counting t into ii)
       (finally (return (simplify expr)))))
    (x x)))

;; gather
(defun reduce-coefficients (terms &aux (tbl (make-hash-table :test 'equal)))
  (iter (for ei in terms)
    (match ei
      ((or (list* '* (guard rest (position-if #'symbolp rest))) (and b (symbol)))
       (letv* ((rest (or rest (list b))))
	 (letv* ((b-position (position-if #'symbolp rest))
		 (b (elt rest b-position))
		 (a (simplify `(* ,@(subseq rest 0 b-position) ,@(subseq rest (1+ b-position))))))
	   (match (gethash b tbl)
	     ((list* '* (and rest (list b-coeff (eql b))))
	      (setf (first rest) (simplify `(+ ,a ,b-coeff))))
	     (_ (collect (setf (gethash b tbl) `(* ,a ,b)) into lin))))))
      (_ (collect ei into c)))
    (finally (return (list* `(+ ,@c) lin)))))

(defun gather (expr)
  (match (simplify expr)
    ((list* '+ rest) `(+ ,@(reduce-coefficients rest)))
    (x x)))

;;
(defun %gse (x)
  (-> x (expand) (simplify) (gather)))

(defun canonicalize-equation (eqn &optional (tbl (make-hash-table)) allow-non-numericp)
  (iter (for ai in (match (%gse eqn)
		     ((list* '+ rest) rest)
		     (x (list 0 x))))
    (collect
	(let ((ai (simplify ai)))
	  (ematch ai
	    ((symbol) (cons (gethash! ai tbl (hash-table-count tbl)) 1))
	    ((list '* (and a (number)) (and b (symbol)))
	     (cons (gethash! b tbl (hash-table-count tbl)) a))
	    ((list '* (and a (number)) (and b (symbol)))
	     (cons (gethash! b tbl (hash-table-count tbl)) a))
	    ((number) ai)
	    (_ (if (not allow-non-numericp)
		   (error "given non-numeric coefficient")
		   (ematch ai
		     ((list* '* (guard rest (find-if #'symbolp rest)))
		      (let ((b (find-if #'symbolp rest)))
			(cons (gethash! b tbl (hash-table-count tbl)) (%gse `(* ,@(remove b rest))))))
		     (_ ai)))))))))

;;
(defun read-polyhedra (constraints &optional tbl allow-non-numericp
		       &aux (tbl (or tbl (make-hash-table))))
  (let ((rows
	  (iter o (for cst in constraints)
	    (ematch cst
	      ((list* (and op (or '<= '= '>=)) rest)
	       (iter (for (a b) on rest) (while b)
		 (letv* (((neg-b . coeffs) (canonicalize-equation `(- ,a ,b) tbl allow-non-numericp))
			 (op (intern (symbol-name op) "KEYWORD")))
		   (in o (collect (list coeffs op (%gse `(- ,neg-b))))))))))))
    (values rows tbl)))

(defun read-lp (c constraints &optional tbl)
  (letv* ((rows tbl (read-polyhedra constraints tbl))
	  ;; remove redundant constraints
	  (rows (remove-if
		 #'(lambda (x)
		     (match x
		       ((list (list (list* _ -1)) :<= 0) t)
		       ((list (list (list* _ 1)) :>= 0) t)))
		 rows))
	  (c-arr (letv* (((c-const &rest c-coo) (canonicalize-equation c tbl))
			 (c-arr (make-array (1+ (hash-table-count tbl)))))
		   (iter (for (j . v) in c-coo) (setf (aref c-arr j) v))
		   (setf (aref c-arr (1- (length c-arr))) c-const)
		   c-arr))
	  (n (length rows))
	  (A-coo nil)
	  (op (make-array n))
	  (b (make-array n)))
    (iter (for (coeff-ii op-ii b-ii) in rows)
      (setf A-coo (concatenate 'list A-coo (mapcar (lambda (x) (list* ii x)) coeff-ii))
	    (aref op ii) op-ii
	    (aref b ii) b-ii)
      (counting t into ii))
    (values (list c-arr A-coo op b) tbl)))

(defun read-fm (constraints &optional tbl (allow-non-numericp t))
  (letv* ((rows tbl (read-polyhedra constraints tbl allow-non-numericp))
	  (n (hash-table-count tbl)))
    (values
     (map 'vector
      #'(lambda (row)
	  (letv* (((coeff-coo op b) row)
		  (coeff-vec (make-array (1+ n)
					 :element-type (if allow-non-numericp t 'u.fm:inequation-dtype)
					 :initial-element 0)))
	    (iter (for (j . v) in coeff-coo) (setf (aref coeff-vec j) v))
	    (setf (aref coeff-vec n) b)
	    (u.fm:make-inequation :row coeff-vec :op op)))
      rows)
     tbl)))

;;
(defun %compile-linear (hplane variables)
  `(- ,(aref hplane (1- (length hplane))) 0
      ,@(iter (for hi in-vector hplane)
	      (for vi in-vector variables)
	      (if (/= hi 0) (collect `(* ,hi ,vi))))))

(defun %compile-projection (ii solution variables)
  (iter (for sol-jj in-vector solution with-index jj)
	(letv* ((op (u.fm:inequation-op sol-jj))
		(hplane (copy-seq (u.fm:inequation-row sol-jj)))
		(pivot (aref hplane ii))
		(gcd (if (= 1 (abs pivot)) 1 (reduce #'gcd hplane :initial-value (abs pivot))))
		(mod (abs (/ pivot gcd))))
	  (let ((zz (* (signum pivot) gcd)))
	    (when (/= 1 zz) (<- (ii) (aref hplane ii)
				(ematch (aref hplane ii)
				  ((number) (/ (aref hplane ii) zz))
				  ((number) `(/ ,(aref hplane ii) ,zz))))))
	  (when (< pivot 1) (setf op (ecase op (:<= :>=) (:>= :<=) (:= :=))))
	  (setf (aref hplane ii) 0)
	  (ecase op
	    (:=
	     (if (/= mod 1) (error "fractional coefficients for equality, ~a" (aref variables ii)))
	     (return (values (list (aref variables ii) hplane (aref variables ii)) t)))
	    (:<= (collect `(floor ,(%compile-linear hplane variables) ,mod) into upper))
	    (:>= (collect `(ceiling ,(%compile-linear hplane variables) ,mod) into lower))))
	(finally
	 (assert (not (null upper)) nil "missing upper bound for ~a" (aref variables ii))
	 (assert (not (null lower)) nil "missing lower bound for ~a" (aref variables ii))
	 (return `(loop :for ,(aref variables ii) :of-type fixnum :from (max ,@lower) :to (min ,@upper))))))

(defun %splice-equalities (elim variables equalities &optional interiorp)
  (letv* ((ii (position elim variables)))
    (iter (for eq.i in equalities)
	  (letv* (((var hplane . stack) eq.i))
	    (when (not (eq 0 (aref hplane ii)))
	      (when (eq var 'of) elim)
	      (letv* ((step `(- ,(aref hplane ii)))
		      (current-offset (first stack))
		      (next-offset (gensym (format nil "offset-~a@~a" var elim)))
		      (nil (progn (setf (aref hplane ii) 0)
				  (push next-offset (cddr eq.i))))
		      (donep (every #'(lambda (x) (eq x 0)) (subseq hplane 0 (1- (length hplane))))))
		(if interiorp
		    (progn
		      (appending `(:with ,current-offset :of-type fixnum := (+ (* ,step ,elim) ,(if donep (aref hplane (1- (length hplane))) next-offset))) into head)
		      (appending `(:do (incf ,current-offset ,step)) into tail))
		    (collect `(,current-offset (* ,step ,elim) :type fixnum) into head)))))
	  (finally (return (values head tail))))))

(defun %compile-convex (form inequations)
  (flet ((%parse-fm (eqns)
	   (letv* ((ret tbl (read-fm eqns))
		   (variable-table (make-array (hash-table-count tbl))))
	     (maphash (lambda (k v) (setf (aref variable-table v) k)) tbl)
	     (values ret variable-table))))
      (match form
	((lambda-list 'do-polyhedron (λlist var &rest constraints) &rest body)
	 (letv* ((new-inequations (cons constraints inequations))
		 (transformed-body system (%compile-convex `(locally ,@body) new-inequations))
		 ((projection variables &optional equalities) (or system (multiple-value-list (%parse-fm (apply #'append new-inequations)))))
		 (ii (position var variables))
		 (nil (unless ii (error "missing bounds for ~a" var)))
		 (next-projection solution (u.fm:fourier-motzkin ii projection))
		 (compiled-solution equalityp (%compile-projection ii solution variables)))
	   (if equalityp
	       (letv* ((eq-decl (%splice-equalities var variables equalities nil))
		       (transformed-body (if eq-decl
					     `(letv* (,@eq-decl) ,transformed-body)
					     transformed-body)))
		 (push compiled-solution equalities)
		 (values transformed-body (list next-projection variables equalities)))
	       ;; eliminating an interior variable
	       (letv* ((loop-body compiled-solution)
		       (eq-head eq-tail (%splice-equalities var variables equalities t))
		       (transformed-body (append loop-body eq-head `(:do ,transformed-body) eq-tail)))
		 (values transformed-body (list next-projection variables equalities))))))
	((type list)
	 (iter (for fi in form) (with ret-system = nil)
	   (letv* ((transformed.fi system (%compile-convex fi inequations)))
	     (if ret-system
		 (assert (null system) nil "polyhedron not convex")
		 (setf ret-system system))
	     (collect transformed.fi into code))
	   (finally (return (values code ret-system)))))
	(_ form))))

;;
(defmacro do-polyhedron (&whole form (var &rest constraints) &rest body)
  (%compile-convex form nil))
