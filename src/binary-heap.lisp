;; Copyright (c) 2020 Akshay Srinivasan <akssri@vakra.xyz>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package #:upakarana-binary-heap)

(declaim (inline parent-index))
(defun parent-index (idx)
  (the fixnum (floor (1- idx) 2)))

(declaim (inline lchild-index))
(defun lchild-index (idx)
  (the fixnum (+ 1 (the fixnum (* 2 idx)))))

(declaim (inline heapify-up))
(defun heapify-up (idx vec &optional (order #'<) map-hook)
  (declare (type fixnum idx))
  (loop
    (if (< 0 idx)
	(let ((p-idx (parent-index idx)))
	  (if (funcall order (aref vec p-idx) (aref vec idx)) (return)
	      (progn
		(when map-hook
		  (funcall map-hook idx p-idx)
		  (funcall map-hook p-idx idx))
		(rotatef (aref vec p-idx) (aref vec idx))
		(setf idx p-idx))))
	(return))))

(declaim (inline heapify-down))
(defun heapify-down (idx vec &optional (order #'<) map-hook)
  (declare (type fixnum idx))
  (loop
    (let* ((l-idx (lchild-index idx))
	   (r-idx (1+ l-idx))
	   (swap-idx idx))
      (when (and (< l-idx (length vec)) (not (funcall order (aref vec swap-idx) (aref vec l-idx))))
	(setf swap-idx l-idx))
      (when (and (< r-idx (length vec)) (not (funcall order (aref vec swap-idx) (aref vec r-idx))))
	(setf swap-idx r-idx))
      (if (= swap-idx idx) (return)
	  (progn
	    (when map-hook
	      (funcall map-hook idx swap-idx)
	      (funcall map-hook swap-idx idx))
	    (rotatef (aref vec idx) (aref vec swap-idx))
	    (setf idx swap-idx))))))

(declaim (inline peek))
(defun peek (vec)
  "(PEEK vec) => NIL or extreme-value
  Return, if it exists, the extreme element; O(1)."
  (when (< 0 (length vec))
    (aref vec 0)))

(declaim (inline push))
(defun push (item vec &optional (order #'<) map-hook)
  "(PUSH item vec) => vec
  INSERT. Insert @arg{item} into heap; O(log n)."
  (heapify-up (vector-push-extend item vec) vec order map-hook)
  vec)

(declaim (inline pop))
(defun pop (vec &optional (order #'<) map-hook)
  "(POP vec) => extreme-value
  This operation extricates the extremum node from the heap; O(log n)."
  (when (< 0 (length vec))
    (when map-hook
      (funcall map-hook 0 -1)
      (funcall map-hook (1- (length vec)) 0))
    (rotatef (aref vec 0) (aref vec (1- (length vec))))
    (prog1 (vector-pop vec)
      (heapify-down 0 vec order map-hook))))

(declaim (inline update-node))
(defun update-node (new idx vec &optional (order #'<) map-hook)
  (let ((old (aref vec idx)))
    (setf (aref vec idx) new)
    (if (funcall order new old)
	(heapify-up idx vec order map-hook)
	(heapify-down idx vec order map-hook)))
  new)

(declaim (inline make-heap))
(defun make-heap (seq &optional (order #'<) map-hook)
  (let ((vec (make-extensible-vector)))
    (map nil #'(lambda (x) (vector-push-extend x vec)) seq)
    (loop :for ii :from (parent-index (1- (length seq))) :downto 0
       :do (heapify-down ii vec order map-hook))
    vec))
