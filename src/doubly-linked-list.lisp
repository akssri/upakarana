;; Copyright (c) 2020 Akshay Srinivasan <akssri@vakra.xyz>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package #:upakarana-doubly-linked-list)
;; (setf *print-circle* t)

(defstruct (dcons (:conc-name d))
  "[<car> <rdc> <cdr>]"
  car rdc cdr)

(defun dcons! (obj rdc cdr)
  "(DCONS! obj rdc cdr)
   creates a new dcons cell, and populates its slots such that the inversion property is preserved.
   (cdr (rdc x)) => x
   (rdc (cdr x)) => x
   this function also modifies adjoining dcons cells to preserve this."
  (let ((ret (make-dcons :car obj :rdc rdc :cdr cdr)))
    (typecase rdc (dcons (setf (dcdr rdc) ret)))
    (typecase cdr (dcons (setf (drdc cdr) ret)))
    ret))

(defun dlist (&rest objs &aux (ret (dcons! (car objs) nil nil)))
  "(DLIST *objs)
   creates a doubly-linked list from `objs`; ends are free."
  (do ((objs (cdr objs) (cdr objs))
       (lst ret (dcons! (car objs) lst nil)))
      ((null objs) ret)))

(defun dcircle (&rest objs &aux (ret (dcons! (car objs) nil nil)))
  "(DCIRCLE *objs)
   creates a circular doubly-linked list from `objs`.
   creates d.l. list and joins the ends to make a circle."
  (setf (drdc ret) ret (dcdr ret) ret)
  (do ((objs (cdr objs) (cdr objs))
       (lst ret (dcons! (car objs) lst ret)))
      ((null objs) ret)))

;; iterate macros
(defmacro-clause (FOR var ON-DLIST dlst &optional IN-REVERSE reversep UNTIL end-dcons)
  "(FOR var ON-DLIST dlst &optional IN-REVERSE reversep UNTIL end-dcons)
   iterates on `dlst` recurively by `dcdr`/`drdc` (based on `reversep`).
   symbol `var` attains values of `dcons` cell at current iteration.

   iteration ends if next-iterate of `var` is a not-dcons value, else if
   `end-dcons` is set, until after `var` attains `end-dcons`."
  (with-gensyms (%dlst %end %next %finishp)
    `(progn
       (with ,%dlst = ,dlst) (with ,%next = nil)
       ,@(if end-dcons `((with ,%end = ,end-dcons) (with ,%finishp = nil)))
       (for ,var initially ,%dlst then ,%next)
       (setf ,%next ,(if (not reversep) `(dcdr ,var) `(drdc ,var)))
       ,@(if end-dcons `((if ,%finishp (finish))))
       (typecase ,var
	 ;; finish on next iteration
	 (dcons ,@(if end-dcons `((if (eql ,var ,%end) (setf ,%finishp t)))))
	 (t (finish))))))

(defmacro-clause (FOR var IN-DLIST dlst &optional IN-REVERSE reversep UNTIL end-dcons)
  "(FOR var IN-DLIST dlst &optional IN-REVERSE reversep UNTIL end-dcons)
   same as `ON-DLIST` except `var` attains values of the `dcar` of current dcons head."
  (with-gensyms (%var*)
    `(progn
       (for ,%var* on-dlist ,dlst in-reverse ,reversep until ,end-dcons)
       (for ,var = (dcar ,%var*)))))

(defun dlast (dlst &optional reversep)
  "(DLAST dlst [reversep])
   returns the last element of d.l. list"
  (if (not reversep)
      (iter (for %x on-dlist dlst)
	(for %xp previous %x)
	(finally (return %xp)))
      (iter (for %x on-dlist dlst in-reverse t)
	(for %xp previous %x)
	(finally (return %xp)))))

;;
(defmacro dpush (obj place &environment env)
  "(DPUSH obj place)
   create new dcons cell with obj as car; melds the cell with place's cdr;
   object at place's rdc is handled as in DCONS!

   setf s place to new-cell"
  (multiple-value-bind (dummies vals new setter getter) (get-setf-expansion place env)
    (when (cdr new) (error "can't expand this."))
    (with-gensyms (ncon)
      (let ((new (car new)))
	`(let* (,@(zip dummies vals)
		(,new ,getter)
		(,ncon (dcons! ,obj
			       (typecase ,new
				 (dcons (drdc ,new))
				 (t nil))
			       ,new)))
	   (setf ,new ,ncon)
	   ,setter)))))

(defmacro dpop (place &optional reversep &environment env)
  "(DPOP place [reversep]) -> value

   pops the dcons cell at `place`; joins `dcdr`/`drdc` values; sets place to either left/right based on `reversep`.
   (? | a | place) (left | b | right) (place | c | ?) -> (? | a | right) (left | c | ?)
   unlike `pop` this modifies the `dcdr` (and its opposite `drdc`) values.

   finally returns `dcar` at `place`."
  (multiple-value-bind (dummies vals new setter getter) (get-setf-expansion place env)
    (when (cdr new) (error "can't expand this."))
    (with-gensyms (left right)
      (let ((new (car new)))
	`(let* (,@(zip dummies vals)
		(,new ,getter))
	   (when ,new
	     (let* ((,left (drdc ,new))
		    (,right (dcdr ,new)))
	       (prog1 (dcar ,new)
		 (typecase ,left (dcons (setf (dcdr ,left) ,right)))
		 (typecase ,right (dcons (setf (drdc ,right) ,left)))
		 (if ,reversep
		     (setf ,new ,left)
		     (setf ,new ,right))
		 ,setter))))))))

;; stack
(defun make-stack ()
  "(MAKE-STACK) -> stack
   stack is a d.l.l structured as follows
   (#1 | <n> | - | e_0 | - | e_1 | .... - | e_{n-1} | #1)
   index indicates 'time' of insertion."
  (u.dlist:dcircle 0))

(defun stack-size (stack)
  "(STACK-SIZE stack) -> size
   returns the number of elements in `stack`."
  (u.dlist:dcar stack))

(defun stack-push! (obj stack)
  "(STACK-PUSH! obj stack) -> stack
   push `obj` onto `stack`."
  (incf (u.dlist:dcar stack))
  (u.dlist:dpush obj (u.dlist:dcdr stack))
  stack)

(defun stack-peek (stack &optional (n 0) &aux (end stack) (size (stack-size stack)))
  "(STACK-PEEK stack [n 0]) -> [value cell]
   peek into the stack at position `n` (can take negative value).

   if `(<= (- size) n (1- size))` is not true raises restartable error
   of type `upakarana:index-out-of-bounds-error`, with place `(n)`."
  (labels ((dcdr-n (v n)
	     (cond
	       ((= n 0) v)
	       ((< 0 n) (dcdr-n (u.dlist:dcdr v) (- n 1)))
	       ((> 0 n) (dcdr-n (u.dlist:drdc v) (+ n 1))))))
    (assert (<= (- size) n (1- size)) (n) 'upakarana:index-out-of-bounds-error :index n :bound size)
    (let ((v (cond
	       ((= n 0) (u.dlist:dcdr stack))
	       ((= n -1) (u.dlist:drdc end))
	       (t (dcdr-n end (if (<= 0 n) (1+ n) n))))))
      (values (u.dlist:dcar v) v))))

(defun stack-pop! (stack &optional (n 0))
  "(STACK-POP! stack [n 0]) -> value
   like stack-peek except the cell is removed from the stack and only its value is returned."
  (letv* ((_ v (stack-peek stack n) :type nil))
    (decf (u.dlist:dcar stack))
    (u.dlist:dpop v (< n 0))))
