;; Copyright (c) 2020 Akshay Srinivasan <akssri@vakra.xyz>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package #:UPAKARANA-GRAPH)

(deftype graph-itype () 'fixnum)

(defstruct graph
  (ptr (make-array 0 :element-type 'graph-itype) :read-only t :type (simple-array graph-itype (*)))
  (idx (make-array 0 :element-type 'graph-itype) :read-only t :type (simple-array graph-itype (*))))

(declaim (inline graph-n))
(defun graph-n (g)
  "(GRAPH-N g) -> n
   returns the number of vertices of `g`, `n`"
  (declare (type graph g))
  (1- (length (graph-ptr g))))

(declaim (inline graph-fence))
(defun graph-fence (g &optional idx)
  "(GRAPH-fence g) -> ptr array
   (GRAPH-fence g u) -> ptr[u] ptr[u+1]"
  (declare (type graph g))
  (etypecase idx
    (null (graph-ptr g))
    (graph-itype
     (let* ((ptr (graph-ptr g)))
       (values (aref ptr idx) (aref ptr (1+ idx)))))))

(declaim (inline graph-neighbors))
(defun graph-neighbors (g &optional u v)
  "(GRAPH-neighbors g) -> (graph-idx g)
   (GRAPH-fence g u) -> (graph-idx g)[lo:hi], where lo, hi are idx-bounds of u
   (GRAPH-fence g u v) -> ((uv \in g)  (bisect_idx of v in N_u))"
  (declare (type graph g))
  (cart-etypecase (u v)
    ((null null) (graph-idx g))
    ((graph-itype null)
     (letv* ((lo hi (graph-fence g u))
	     (ret (make-array (- hi lo) :element-type 'graph-itype)))
       (loop :for ii :below (length ret)
	     :do (setf (aref ret ii) (aref (graph-idx g) (+ ii lo))))
       ret))
    ((graph-itype graph-itype)
     (letv* ((lo hi (graph-fence g u))
	     (idx (bisect v (graph-idx g) :lo lo :hi hi :order #'<)))
       (values (and (< idx hi) (= (aref (graph-idx g) idx) v)) idx)))))

;; display graph
(defun graph->dot (stream g &optional self-loops node-name)
  (if (null stream)
      (with-output-to-string (out) (graph->dot out g))
      (progn
	(format stream "digraph G{~%graph [];~%node[shape=point];~%~%")
	(iter (for u from 0 below (graph-n g))
	  (format stream "~a [xlabel=\"~a\"];~%" u (typecase node-name
						     (function (funcall node-name u))
						     (sequence (elt node-name u))
						     (null "") (t u))))
	(iter (for u from 0 below (graph-n g))
	  (iter (for v in-vector (graph-idx g) from (aref (graph-ptr g) u) below (aref (graph-ptr g) (1+ u)))
	    (when (or (and (= u v) (not self-loops)) (and (< v u) (graph-neighbors g v u))) (next-iteration))
	    (format stream "~a->~a [~alabel=\"\",];~%" u v (if (graph-neighbors g v u) "dir=none,color=red," ""))))
	(format stream "}~%"))))

(defun display-graph (graph &optional self-loops (node-name t)
		      &aux (name (symbol-name (gensym)))
			   (dot-name (format nil "/tmp/tmp-~a.out" name))
			   (svg-name (format nil "/tmp/tmp-~a.svg" name)))
  (with-open-file (out dot-name :direction :output :if-exists :supersede :if-does-not-exist :create)
    (graph->dot out graph self-loops node-name))
  (with-open-file (out svg-name :direction :output :if-exists :supersede :if-does-not-exist :create)
    (external-program:run "/usr/bin/neato" (list dot-name "-Tsvg") :output out))
  (bt:make-thread #'(lambda () (external-program:run "/usr/bin/inkview" (list svg-name) :output nil :wait nil)))
  graph)

;; adjacency list
(defun graph->adlist (g)
  (letv* ((ptr (graph-ptr g)) (idx (graph-idx g))
	  (ret (make-array (graph-n g) :initial-element nil)))
    (iter (for ii below (1- (length ptr)))
      (iter (for jj in-vector idx from (aref ptr ii) below (aref ptr (1+ ii)))
	(push jj (aref ret ii))))
    ret))

(defun adlist->graph (adj)
  (letv* ((adj-sorted (map 'list #'(lambda (x) (sort (coerce x '(simple-array graph-itype (*))) #'<)) adj))
	  (ptr (cumsum (map '(simple-array graph-itype (*)) #'length adj)))
	  (idx (apply #'concatenate '(simple-array graph-itype (*)) adj-sorted)))
    (make-graph :ptr ptr :idx idx)))

(defun symmetrize-adlist (adj)
  (iter (for ad.i in-vector adj with-index i)
    (iter (for j in ad.i)
      (push i (aref adj j))))
  (iter (for ad.i in-vector adj with-index i)
    (setf (aref adj i) (remove-duplicates (sort ad.i #'<))))
  adj)

(defun transpose-adlist (adj)
  (letv* ((adj.t (make-array (length adj) :initial-element nil)))
    (iter (for ad.i in-vector adj with-index i)
      (iter (for j in ad.i) (push i (aref adj j))))
    adj.t))

(defun permutation->tree (p)
  "(permutation->TREE order) -> g
   `p` is a permutation of [0 .. (length `p`))"
  (let ((ptr (make-array (1+ (length p)) :initial-element 0 :element-type 'graph-itype))
	(idx (make-array (length p) :initial-contents p :element-type 'graph-itype)))
    (iter (for ii below (length p)) (setf (aref ptr (1+ ii))  (1+ ii)))
    (make-graph :ptr ptr :idx idx)))

;; benchmark: (random-gnp 10000 0.05), edge-weights from (u.random:exponential)
;; fib-heap                  | 0.70 s
;; binary-heap (+ v-update)  | 1.00 s
;; binary-heap               | 3.74 s
(defun dijkstra (src neighbors &key dest repushp)
  "(DIJKSTRA src neighbors [dest] [repushp]) => {u: (g-u prev-u node-u) ...}
  generic djikstra algorithm.

  - `src` the start node
  - (`neighbours` u g.u dest) => closure,
    each call of which outputs (list v g.v &optional (h.v 0)) for a neighbor node.
  - returns a hashtable mapping states to  (list* g-u prev-u node-u)
  - if node-u is nil then g-u is optimal if the heuristic is consistent,
    h(x) ≤ d(x, y) + h(y),
    or if u is on the path from @arg{src} to @arg{dest}

  - Dijkstra:       g.v = g.u + w_{uv}
  - A*:             g.v = g.u + w_{uv}, h.v = heuristic of d(v, dest), repushp = T
  - Dijkstra-Prims: g.v = 1"
  (letv* ((queue (make-instance 'u.fheap:fibonacci-heap :order #'(lambda (x y) (< (first x) (first y)))))
	  (dist (make-hash-table :test 'equalp)))
    (letv* ((_ src-node (u.fheap:push (list* 0 src) queue))) ;; queue holds (g.u + h.u . u)
      (setf (gethash src dist) (list* 0 src src-node)))	     ;; dist holds  v: (g.v prev-v . v-fib-node)
    (iter (while (< 0 (u.fheap:size queue)))
      (letv* ((u (cdr (u.fheap:pop queue))))
	(if (eql u dest) (finish)
	    (letv* ((g.u (let ((entry.x (gethash u dist)))
			   (setf (cddr entry.x) nil) ;; clear fib-node
			   (first entry.x)))
		    (u.n* (funcall neighbors u g.u dest)))
	      (iter (for node next (funcall u.n*)) (while node)
		(letv* (((x g.x &optional (h.x 0)) node)
			(entry.x existsp.x (gethash! x dist (list g.x u))) ;; node.x is nil
			((g-prev.x _ . node.x) entry.x))
		  (when (or (not existsp.x) (and (or repushp node.x) (< g.x g-prev.x)))
		    (setf (values (first entry.x) (second entry.x)) (values g.x u))
		    (if node.x
			(u.fheap:update-node (list* (+ g.x h.x) x) node.x queue)
			(letv* ((_ node.x (u.fheap:push (list* (+ g.x h.x) x) queue)))
			  (setf (cddr entry.x) node.x))))))))))
    dist))

;; ;; binary-heap version
;; (defun dijkstra (src neighbors &optional dest)
;;   (letv* ((queue (make-extensible-vector))
;; 	  (dist (make-hash-table :test 'equalp)))
;;     (vector-push-extend (list src 0) queue)  ;; (g.v + h.v, u)
;;     (setf (gethash src dist) (list src 0 0)) ;; v: (g.v, prev-v, ptr-v)
;;     (flet ((order (x y) (< (first x) (first y)))
;; 	   (map-hook (i new-i)
;; 	     (let* ((u.i-entry (gethash (second (aref queue i)) dist)))
;; 	       (setf (third u.i-entry) new-i))))
;;       (iter (while (< 0 (length queue)))
;; 	(letv* ((u (second (u.bheap:pop queue #'order #'map-hook))))
;; 	  (if (eql u dest) (finish)
;; 	      (letv* ((g.u (first (gethash u dist)))
;; 		      (u.n* (funcall neighbors u g.u dest)))
;; 		(iter (for node next (funcall u.n*)) (while node)
;; 		  (letv* (((x g.x &optional (h.x 0)) node)
;; 			  (entry.x existsp.x (gethash! x dist (list g.x u -1)))
;; 			  ((g-prev.x prev.x ptr.x) entry.x))
;; 		    (when (or (not existsp.x) (< g.x g-prev.x))
;; 		      (setf (first entry.x) g.x (second entry.x) u)
;; 		      (if (>= ptr.x 0)
;; 			  (let ((qentry.x (aref queue ptr.x)))
;; 			    (setf (first qentry.x) (+ g.x h.x))
;; 			    (u.bheap:heapify-up ptr.x queue #'order #'map-hook))
;; 			  (u.bheap:push (list (+ g.x h.x) x) queue #'order #'map-hook))))))))))
;;     dist))

;; DFS
(declaim (inline %push-stack!))
(defun %push-stack! (v order g visitedp-table stack!)
  (letv* ((lo hi (graph-fence g v)))
    (if (eql order :dfs) (u.dlist:stack-push! (cons v t) stack!)) ;; mark start of stack.
    (iter (for u in-vector (graph-idx g) from lo below hi)
      (with num-visited = 0)
      (if (aref visitedp-table u) (incf num-visited)
	  (u.dlist:stack-push! (cons u v) stack!))
      (finally
       (if (eql order :bfs) (u.dlist:stack-push! (cons v t) stack!)) ;; mark end of stack.
       (return num-visited)))))

(declaim (inline %pop-stack!))
(defun %pop-stack! (order g visitedp-table stack! &aux (num-unroll 0))
  (handler-case
      (iter (repeat (graph-n g))
	(letv* (((v-new . p-new) (u.dlist:stack-pop! stack! (ecase order (:dfs 0) (:bfs -1)))))
	  (if (eql p-new t) (incf num-unroll)
	      (unless (aref visitedp-table v-new)
		(return (values num-unroll v-new p-new))))))
    (upakarana:index-out-of-bounds-error () num-unroll)))

(defmacro-clause (FOR v IN-GRAPH g &optional FROM root IN-ORDER order WITH-PARENT parent WITH-NUM-UNROLL num-unroll USING-VISITED-ARRAY visited-array)
  "(FOR 
     v [symbol] : bound-symbol
     IN-GRAPH g [graph] :
    &optional
     FROM root [graph-itype] : start node ; if not given picks random vertex.
     IN-ORDER order [(member (:dfs :bfs)) = :dfs]
     WITH-PARENT parent [symbol] : symbol bound to parent of `v` (NIL if root).
     WITH-NUM-UNROLL num-unroll [symbol]: indicates number of nodes 'unrolled' in the stack i.e num. whose child* enumeration is complete.
     USING-VISITED-ARRAY visited-array [(simple-array boolean (#1)); #1 = (graph-n `g`)]: uses this array to keep track of visited nodes."
  (with-static-gensyms (gm)
    (letv* ((order (or order :dfs)))
      (check-type v symbol)
      `(progn
	 (with ,(gm g) = (the graph ,g))
	 (repeat (graph-n ,(gm g)))	 
	 ;; start node
	 (with ,v = (the graph-itype ,(or root `(random (graph-n ,(gm g))))))
	 ;; visited table
	 (with ,(gm visited-table) = ,(or visited-array `(make-array (graph-n ,(gm g)) :element-type 'boolean :initial-element nil)))
	 ;; quits if node has been visited
	 (initially (if (aref ,(gm visited-table) ,v) (return)
			(setf (aref ,(gm visited-table) ,v) t)))
	 ;; track parent
	 ,@(if parent `((with ,parent = nil)))
	 ;; track stack-unroll
	 ,@(if num-unroll `((with ,num-unroll = 0)))
	 ;; stack
	 (with ,(gm stack) = (u.dlist:make-stack))
	 ;; types
	 (declare (type graph ,(gm g))
		  (type (simple-array boolean (*)) ,(gm visited-table)))
	 ;; iterate
	 (after-each
	  (%push-stack! ,v ,order ,(gm g) ,(gm visited-table) ,(gm stack))
	  (letv* ((,(gm num-unroll) ,(gm v-new) ,(gm p-new) (%pop-stack! ,order ,(gm g) ,(gm visited-table) ,(gm stack))))
	    (unless ,(gm v-new) (finish))
	    (setf ,v ,(gm v-new))
	    (setf (aref ,(gm visited-table) ,v) t)
	    ,@(if parent `((setf ,parent ,(gm p-new))))
	    ,@(if num-unroll `((setf ,num-unroll ,(gm num-unroll))))))))))

;;
(defun topological-order (dag)
  "(TOPOLOGICAL-ORDER dag) -> topological-order, dagp
   dag [graph]:

   topological-order is a permutation of [0 ... n-1] such that uv \in G \implies order[u] < order[v].
   uses DFS to compute order."
  (declare (type graph dag))
  (let* ((visited (make-array (graph-n dag) :element-type 'boolean :initial-element nil))
	 ;; read out 
	 (order (make-array (graph-n dag) :element-type 'graph-itype :initial-element 0))
	 (order-push (let ((%order-ii (length order)))
		       (lambda (u) (setf (aref order (decf %order-ii)) u))))
	 ;; dagp
	 (dagp t))
    (iter (for u below (graph-n dag))
      (let ((stack nil)
	    (mask (make-array (graph-n dag) :element-type 'boolean :initial-element nil)))
	(iter (for tu in-graph dag from u in-order :dfs with-num-unroll n-unroll using-visited-array visited)
	  ;; dag check
	  (when dagp
	    (setf (aref mask tu) t)
	    (letv* ((lo hi (graph-fence dag tu)))
	      ;; if nodes on the top of stack are reachable then not a dag
	      (iter (for tv in-vector (graph-idx dag) from lo below hi)
		(when (aref mask tv)
		  (setf dagp nil)
		  (return)))))
	  ;; track current path
	  (push tu stack)
	  (iter (repeat n-unroll) (funcall order-push (pop stack))))
	;; unroll rest
	(iter (while stack) (funcall order-push (pop stack)))))
    (values order dagp)))
